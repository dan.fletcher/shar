package keys

const (
	ElementName              = "el.name"
	ElementID                = "el.id"
	ElementType              = "el.type"
	WorkflowInstanceID       = "wf.iid"
	WorkflowID               = "wf.id"
	JobType                  = "job.type"
	JobID                    = "job.id"
	ParentWorkflowInstanceID = "wf.parent.iid"
	WorkflowName             = "wf.name"
	ParentInstanceElementID  = "wf.parent.el.id"
	Execute                  = "wf.ex"
	MessageID                = "msg.id"
	Condition                = "el.cond"
	State                    = "el.state"
	TrackingID               = "wf.trid"
	ParentTrackingID         = "wf.tpid"
)
