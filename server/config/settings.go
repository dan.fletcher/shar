package config

import (
	"github.com/caarlos0/env/v6"
)

type settings struct {
	Port          int    `env:"SHAR_PORT" envDefault:"50000"`
	NatsURL       string `env:"NATS_URL" envDefault:"nats://127.0.0.1:4222"`
	LogLevel      string `env:"SHAR_LOG_LEVEL" envDefault:"debug"`
	PanicRecovery bool   `env:"SHAR_PANIC_RECOVERY" envDefault:"true"`
}

func GetEnvironment() (*settings, error) {
	cfg := &settings{}
	if err := env.Parse(cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
